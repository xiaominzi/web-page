var log4js = require('log4js'),
    config = require('../config'),
    logger = log4js.getLogger(config.logger.name);

logger.setLevel(config.logger.level);

module.exports = logger;
