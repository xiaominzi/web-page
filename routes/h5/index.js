var router = require('express').Router();

//幼小衔接介绍页
router.get('/h5/youxiao', function (req, res) {
    res.render('h5/youxiao');
});

router.get('/h5/activity', function (req, res) {
    res.render('h5/xie');
});
router.get('/h5/comment', function (req, res) {
    res.render('h5/comment');
});
router.get('/h5/new', function (req, res) {
    res.render('h5/new');
});
router.get('/h5/scroll', function (req, res) {
    res.render('h5/index');
});
router.get('/home', function (req, res) {
    res.render('h5/home');
});
router.get('/swiper', function (req, res) {
    res.render('h5/swiper');
});
//万年历
router.get('/newindex', function (req, res) {
    res.render('h5/newindex');
});
//万年历
router.get('/course', function (req, res) {
    res.render('h5/course');
});
//月
router.get('/month', function (req, res) {
    res.render('h5/Month');
});
router.get('/test', function (req, res) {
    res.render('h5/test');
});
router.get('/circle', function (req, res) {
    res.render('h5/yuanquan');
});


//顶部tan切换
router.get('/tab', function (req, res) {
    res.render('h5/xiaomin03');
});
//tab 选择切换
router.get('/TabDrag', function (req, res) {
    res.render('h5/TabDrag');
});
//表单?
router.get('/form', function (req, res) {
    res.render('h5/form');
});
//echarts
router.get('/charts', function (req, res) {
    res.render('h5/Echarts');
});
//励步首页
router.get('/lbindex', function (req, res) {
    res.render('h5/badaoma');
});




//个人中心
router.get('/PersonalCenter', function (req, res) {
    res.render('h5/PersonalCenter');
});
//新建地址 over
router.get('/address', function (req, res) {
    res.render('h5/address');
});
//我的订单 over
router.get('/order', function (req, res) {
    res.render('h5/order');
});
//订单详情 over
router.get('/ToCopeWith', function (req, res) {
    res.render('h5/ToCopeWith');
});
//优惠券
router.get('/discount', function (req, res) {
    res.render('h5/discount');
});
//优惠券
router.get('/Testrem', function (req, res) {
    res.render('h5/Testrem');
});

//网易严选
router.get('/netease', function (req, res) {
    res.render('h5/netease');
});
//网易严选详情页
router.get('/neteaseParticulars', function (req, res) {
    res.render('h5/neteaseParticulars');
});
//优惠卷
router.get('/coupons', function (req, res) {
    res.render('h5/coupons');

//左滑动删除
router.get('/sliderDelete', function (req, res) {
    res.render('h5/sliderDelete');
});

//秒杀
router.get('/qwe', function (req, res) {
    res.render('h5/qwe');

});

module.exports = router;

