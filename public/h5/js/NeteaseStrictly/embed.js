!function(e) {
    function t(o){if(n[o])return n[o].exports;var r=n[o]={i: o,l:!1,exports:{
    }};

        return e[o].call(r.exports,r,r.exports,t),r.l=!0,r.exports
    }

    var n= {
    };

    t.m=e,t.c=n,t.i=function(e) {
        return e
    },t.d=function(e,n,o) {
        t.o(e,n)||Object.defineProperty(e,n,{configurable: !1,enumerable:!0,get:o
        })},t.n=function(e) {
        var n=e&&e.__esModule?function(){return e.default
            }

            :function() {
                return e
            };

        return t.d(n,"a",n),n
    },t.o=function(e,t) {
        return Object.prototype.hasOwnProperty.call(e,t)
    },t.p="",t(t.s=45)
}({1:function(e,t,n) {
    var o={sharerKey: "ts_sharerId",cpsKey:"ts_dealer",fromShareKey:"ts_share",aKey:"ts_a",bindAccount:"ts_bind_account",bindSharer:"ts_bind_sharer",isTsKey:"ts_isTs",cookieMaxAge:43200,baseUrl:"",redirectUrl:"",visitorBindUrl:"/fan/bind",isSelfUrl:"/fan/self",shortLinkUrl:"/openapi/shortlink",productUrl:"/openapi/goods",sharerInfoUrl:"/sharer/isQualified",kaolapvUrl:"/openapi/sa?event=kaolapv",yanxuanpvUrl:"/openapi/sa?event=yanxuanpv",clickTrackUrl:"/openapi/sa?event=goodsButtonClick",screenshotIframe:"/_testPage/screenshot.html",yxscreenshotIframe:"/config/yxscreenshot.html",loadJS:{regularjs:"//ts.163.com/static/3rd/js/3rd/regular.js",qrcode:"//ts.163.com/static/3rd/js/3rd/qrcode.js",messenger:"//ts.163.com/static/3rd/js/3rd/messenger.js",promise:"//ts.163.com/static/3rd/js/3rd/promise.js"
    }};

    o.screenshotIframe="/res/service/screenshot/wechatbiz/index.html",o.yxscreenshotIframe="/html/yxscreenshot.html",o.baseUrl="//wx.ts.163.com",o.redirectUrl="https://wx.ts.163.com/sharer/special?ou=",e.exports=o
},10:function(e,t,n) {
    var o,r;!function(a){var s=!1;if(o=a,void 0!==(r="function"==typeof o?o.call(t,n,t,e): o)&&(e.exports=r),s=!0,e.exports=a(),s=!0,!s){var i=window.Cookies,c=window.Cookies=a();
        c.noConflict=function(){return window.Cookies=i,c
        }}}(function() {
        function e(){for(var e=0,t={
        };

                         e<arguments.length;e++) {
            var n=arguments[e];for(var o in n)t[o]=n[o]
        }

            return t
        }

        function t(n) {
            function o(t,r,a){var s;if("undefined"!=typeof document){if(arguments.length>1){if(a=e({path: "/"
                },o.defaults,a),"number"==typeof a.expires) {
                var i=new Date;i.setMilliseconds(i.getMilliseconds()+864e5*a.expires),a.expires=i
            }

                a.expires=a.expires?a.expires.toUTCString():"";try {
                    s=JSON.stringify(r),/^[\{\[]/.test(s)&&(r=s)
                }

                catch(e) {
                }

                r=n.write?n.write(r,t):encodeURIComponent(String(r)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g,decodeURIComponent),t=encodeURIComponent(String(t)),t=t.replace(/%(23|24|26|2B|5E|60|7C)/g,decodeURIComponent),t=t.replace(/[\(\)]/g,escape);var c="";for(var l in a)a[l]&&(c+="; "+l,!0!==a[l]&&(c+="="+a[l]));return document.cookie=t+"="+r+c
            }

                t||(s= {
                });

                for(var u=document.cookie?document.cookie.split("; "):[],h=0;h<u.length;h++) {
                    var p=u[h].split("="),d=p.slice(1).join("=");'"'===d.charAt(0)&&(d=d.slice(1,-1));try{var f=p[0].replace(/(%[0-9A-Z]{2
                    })+/g,decodeURIComponent);if(d=n.read?n.read(d,f):n(d,f)||d.replace(/(%[0-9A-Z] {
        2
    })+/g,decodeURIComponent),this.json)try {
    d=JSON.parse(d)
}

    catch(e) {
    }

    if(t===f) {
    s=d;break
}

t||(s[f]=d)
}

catch(e) {
}}

return s
}}

return o.set=o,o.get=function(e) {
    return o.call(o,e)
},o.getJSON=function() {
    return o.apply({json: !0
    },[].slice.call(arguments))
},o.defaults= {
},o.remove=function(t,n) {
    o(t,"",e(n,{expires: -1
    }))},o.withConverter=t,o
}

return t(function() {
})})},2:function(e,t,n) {
    var o={
    },r=n(10),a=n(1),s=window.location.host;o.get=function(e,t) {
        return t=t||"sessionStorage",s.indexOf("you.163.com")>-1&&e!==a.bindAccount&&e!==a.bindSharer&&e!==a.isTsKey?r.get(e): window[t].getItem(e)
    },o.set=function(e,t,n) {
        n=n||"sessionStorage",window[n].setItem(e,t),s.indexOf("you.163.com")>-1&&e!==a.bindAccount&&e!==a.bindSharer&&e!==a.isTsKey&&r.set(e,t,{domain: ".163.com","max-age":a.cookieMaxAge
        })},o.remove=function(e,t) {
        t=t||"sessionStorage",window[t].removeItem(e)
    },o.clear=function(e) {
        e=e||"sessionStorage",window[e].clear()
    },e.exports=o
},3:function(e,t) {
    function n(e,t,n,o){var r,a=e.status;if(a>=200&&a<300||304==a){switch(t){case"text": r=e.responseText;
        break;case"json": r=JSON.parse(e.responseText);
        break;case"xml": r=e.responseXML
    }

        n(r)
    }

    else 0==a?o(e,"request timeout"):o(e,e.status);e=null
    }

    function o(e) {
        var t=[];for(var n in e){var o=e[n];t.push(n+"="+encodeURIComponent(o))
        }

        return t.join("&")
    }

    e.exports=function(e,t) {
        var r=function(){
        },a=!1!==t.async,s=t.method||"GET",i=t.dataType||"text",c=t.encode||"UTF-8",l=t.data||null,u=t.timeout||0,h=t.headers||null,p=t.success||r,d=t.failure||r;s=s.toUpperCase(),l&&"object"==typeof l&&(l=o(l)),"GET"==s&&l&&(e+=(-1==e.indexOf("?")?"?":"&")+l,l=null);var f,m=new XMLHttpRequest,g=!1;if(u>0&&(f=setTimeout(function() {
                m.abort(),g=!0
            },u)),m.onreadystatechange=function() {
                4!=m.readyState||g||(n(m,i,p,d),clearTimeout(f))
            },m.open(s,e,a),h)for(var y in h)m.setRequestHeader(y,h[y]);return"POST"===s&&(h&&h["Content-Type"]||m.setRequestHeader("Content-Type","application/x-www-form-urlencoded;charset="+c)),m.withCredentials=!0,m.send(l),m
    }},45:function(e,t,n) {
    function o(e){var t=l(e),n="";if(1e3===t?n=g.baseUrl+g.kaolapvUrl: 1001===t&&(n=g.baseUrl+g.yanxuanpvUrl),n){var o={pageUrl:window.location.href
    },r=s(t);r&&(o.productId=r),m.get(g.sharerKey)&&(o.sharerId=m.get(g.sharerKey)),m.get(g.aKey)&&(o.tsA=m.get(g.aKey)),setTimeout(function() {
        o.pageTitle=document.title,f(n,{method: "post",dataType:"json",data:JSON.stringify(o),headers:y,timeout:1e4,success:function(){
        }})},1e3)
    }}

    function r(e) {
        var t=m.get(g.sharerKey);t&&f(g.baseUrl+g.isSelfUrl,{method: "post",dataType:"json",data:JSON.stringify({s:t
        }),headers:y,timeout:1e4,success:function(t) {
            200===t.code&&(t.result||e&&e())
        }})}

    function a(e) {
        var t,n=p.get("S_INFO"),o=p.get("P_INFO"),a=p.get("S_OINFO"),s=p.get("P_OINFO"),i=m.get(g.sharerKey);if(i){var c;if(n&&o?c=o.split("|"): a&&s&&(c=s.split("|")),c&&c.length){if(t=c[0],m.get(g.bindAccount)===t&&m.get(g.bindSharer)===i)return;
            var l=g.baseUrl;"m.kaola.com"===e||"market.kaola.com"===e?l="//ts.kaola.com": "m.kaola.com.hk"===e&&(l="//ts.kaola.com.hk"),r(function(){f(l+g.visitorBindUrl,{method:"post",dataType:"json",data:JSON.stringify({p:e,s:i
            }),headers:y,timeout:1e4,success:function(e) {
                200===e.code&&(m.set(g.bindAccount,t),m.set(g.bindSharer,i))
            }})})}}}

    function s(e) {
        var t=window.location.href,n=t.split("?")[0],o=d.parseURL(t).params;try{if(1e3===e){var r=n.match(/product\/(\d+).html/);return r?r[1]: null
        }

            return 1001===e&&(/\/item\/detail/.test(n)&&o.id)?o.id:null
        }

        catch(e) {
            return null
        }}

    function i() {
        p.remove("unionID",{domain: ".kaola.com"
        })}

    function c(e) {
        var t=[g.sharerKey,g.cpsKey,g.fromShareKey],n=["//m.kaola.com/weChatbiz/wechatBizSession.html?sessionStr=","//market.kaola.com/weChatbiz/wechatBizSession.html?sessionStr=","//huodong.kaola.com/weChatbiz/wechatBizSession.html?sessionStr="];"m.kaola.com"===e?n=["//m.kaola.com.hk/weChatbiz/wechatBizSession.html?sessionStr=","//market.kaola.com/weChatbiz/wechatBizSession.html?sessionStr=","//huodong.kaola.com/weChatbiz/wechatBizSession.html?sessionStr="]: "market.kaola.com"===e?n=["//m.kaola.com.hk/weChatbiz/wechatBizSession.html?sessionStr=","//m.kaola.com/weChatbiz/wechatBizSession.html?sessionStr=","//huodong.kaola.com/weChatbiz/wechatBizSession.html?sessionStr="]:"huodong.kaola.com"===e&&(n=["//m.kaola.com.hk/weChatbiz/wechatBizSession.html?sessionStr=","//m.kaola.com/weChatbiz/wechatBizSession.html?sessionStr=","//market.kaola.com/weChatbiz/wechatBizSession.html?sessionStr="]);
        var o={
        };

        for(a=0;a<t.length;a++) {
            var r=t[a];m.get(r)&&(o[r]=m.get(r))
        }

        for(var a=0;a<n.length;a++) {
            n[a]+=JSON.stringify(o);var s=document.createElement("iframe");s.src=n[a],s.style.display="none",document.body&&document.body.appendChild(s)
        }}

    function l(e) {
        return e.indexOf("kaola.com")>-1?1e3: e.indexOf("you.163.com")>-1?1001:""
    }

    function u(e) {
        if(!e)return!1;if(!/x{6,
            }/.test(e))return!1;for(var t=0;t<e.length;t++)if("x"!==e[t])return!1;return!0
}

var h,p=n(10),d=n(5),f=n(3),m=n(2),g=n(1),y= {
    Accept: "application/json","Content-Type":"application/json"
};!function() {
    var e=window.location.href,t=d.parseURL(e),n=t.params,r=t.host,s=!1;if(n[g.cpsKey]){m.set(g.cpsKey,n[g.cpsKey]);var l=n[g.sharerKey];if(l&&l.length)if(u(l.toLowerCase())){s=!0;var p=encodeURIComponent(e);d.isInWX()&&(window.location.href=g.redirectUrl+p)
    }

    else m.set(g.sharerKey,l);n[g.aKey]&&m.set(g.aKey,n[g.aKey])
    }

    s||(h=d.getMyId(),h&&(y.openuid=h),"1"===m.get(g.cpsKey)&&(i(),a(r),r.indexOf("kaola.com")>-1&&c(r),o(r)))
}()},5:function(e,t,n) {
    var o=e.exports,r=n(1),a=n(2),s=n(3);o.parseURL=function(e){e=e||window.location.href;var t=document.createElement("a");return t.href=e,{source: e,protocol:t.protocol.replace(":",""),host:t.hostname,port:t.port,query:t.search,file:(t.pathname.match(/\/([^\/?#]+)$/i)||[,""])[1],hash:t.hash.replace("#",""),path:t.pathname.replace(/^([^\/])/,"/$1"),relative:(t.href.match(/tps?:\/\/[^\/]+(.+)/)||[,""])[1],segments:t.pathname.replace(/^\//,"").split("/"),params:function(){var e={
    };

        return t.search.replace(/^\?/,"").split("&").filter(function(e,t) {
            if(""!==e&&e.indexOf("="))return!0
        }).forEach(function(t,n) {
            var o=t.indexOf("="),r=t.substring(0,o),a=t.substring(o+1);e[r]=a
        }),e
    }()}},o.isInWX=function() {
        return/micromessenger/i.test(navigator.userAgent.toLowerCase())
    },o.getMyId=function() {
        return"1"===a.get(r.cpsKey)&&"1"===a.get(r.isTsKey)?a.get(r.sharerKey): ""
    },o.nosUrlHandler=function(e) {
        return e.replace("nosdn.127.net","nos.netease.com")
    },o.calcCommission=function(e,t) {
        var n=100*e*t/100,o=n+"",r=o.split(".");return r&&r[1]&&r[1].length>2?parseFloat(r[0]+"."+r[1].slice(0,2)): n
    },o.handleUrlLogic=function(e) {
        var t=o.parseURL(e),n=t.params;e=e.replace("#"+t.hash,"");var s="?";return e.indexOf("?")>-1&&(s="&"),n.ts_dealer||(e+=s+r.cpsKey+"="+a.get(r.cpsKey)),n.ts_sharerId||(e+="&"+r.sharerKey+"="+a.get(r.sharerKey)),t.host.indexOf("you.163.com")>-1&&!n.channel_type&&(e+="&channel_type=1"),e
    },o.trackClick=function(e) {
        s(r.baseUrl+r.clickTrackUrl,{method: "post",dataType:"json",data:JSON.stringify(e),headers:{Accept:"application/json","Content-Type":"application/json"
        },timeout:1e4,success:function() {
        }})}}});