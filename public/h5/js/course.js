/**
 * Created by Jerry on 17/11/13.
 */
/**
 * Created by Jerry on 17/10/31.
 */
/*
 * 用于记录日期，显示的时候，根据dateObj中的日期的年月显示
 */
    var dateObj = (function(){
    var _date = new Date();    // 默认为当前系统时间
    return {
        getDate : function(){
            return _date;
        },
        setDate : function(date) {
            _date = date;
        }
    };
    })();

    var CalendarData = new Array(100);
    var madd = new Array(12);
    var tgString = "甲乙丙丁戊己庚辛壬癸";
    var dzString = "子丑寅卯辰巳午未申酉戌亥";
    var numString = "一二三四五六七八九十";
    var monString = "正二三四五六七八九十冬腊";
    var weekString = "日一二三四五六";
    var sx = "鼠牛虎兔龙蛇马羊猴鸡狗猪";
    var cYear, cMonth, cDay, TheDate;
    CalendarData = new Array(0xA4B, 0x5164B, 0x6A5, 0x6D4, 0x415B5, 0x2B6, 0x957, 0x2092F, 0x497, 0x60C96, 0xD4A, 0xEA5, 0x50DA9, 0x5AD, 0x2B6, 0x3126E, 0x92E, 0x7192D, 0xC95, 0xD4A, 0x61B4A, 0xB55, 0x56A, 0x4155B, 0x25D, 0x92D, 0x2192B, 0xA95, 0x71695, 0x6CA, 0xB55, 0x50AB5, 0x4DA, 0xA5B, 0x30A57, 0x52B, 0x8152A, 0xE95, 0x6AA, 0x615AA, 0xAB5, 0x4B6, 0x414AE, 0xA57, 0x526, 0x31D26, 0xD95, 0x70B55, 0x56A, 0x96D, 0x5095D, 0x4AD, 0xA4D, 0x41A4D, 0xD25, 0x81AA5, 0xB54, 0xB6A, 0x612DA, 0x95B, 0x49B, 0x41497, 0xA4B, 0xA164B, 0x6A5, 0x6D4, 0x615B4, 0xAB6, 0x957, 0x5092F, 0x497, 0x64B, 0x30D4A, 0xEA5, 0x80D65, 0x5AC, 0xAB6, 0x5126D, 0x92E, 0xC96, 0x41A95, 0xD4A, 0xDA5, 0x20B55, 0x56A, 0x7155B, 0x25D, 0x92D, 0x5192B, 0xA95, 0xB4A, 0x416AA, 0xAD5, 0x90AB5, 0x4BA, 0xA5B, 0x60A57, 0x52B, 0xA93, 0x40E95);
    madd[0] = 0;
    madd[1] = 31;
    madd[2] = 59;
    madd[3] = 90;
    madd[4] = 120;
    madd[5] = 151;
    madd[6] = 181;
    madd[7] = 212;
    madd[8] = 243;
    madd[9] = 273;
    madd[10] = 304;
    madd[11] = 334;

    function GetBit(m, n) {
        return (m >> n) & 1;
    }
    function e2c() {
        TheDate = (arguments.length != 3) ? new Date() : new Date(arguments[0], arguments[1], arguments[2]);
        var total, m, n, k;
        var isEnd = false;
        var tmp = TheDate.getYear();
        if (tmp < 1900) {
            tmp += 1900;
        }
        total = (tmp - 1921) * 365 + Math.floor((tmp - 1921) / 4) + madd[TheDate.getMonth()] + TheDate.getDate() - 38;

        if (TheDate.getYear() % 4 == 0 && TheDate.getMonth() > 1) {
            total++;
        }
        for (m = 0; ; m++) {
            k = (CalendarData[m] < 0xfff) ? 11 : 12;
            for (n = k; n >= 0; n--) {
                if (total <= 29 + GetBit(CalendarData[m], n)) {
                    isEnd = true; break;
                }
                total = total - 29 - GetBit(CalendarData[m], n);
            }
            if (isEnd) break;
        }
        cYear = 1921 + m;
        cMonth = k - n + 1;
        cDay = total;
        if (k == 12) {
            if (cMonth == Math.floor(CalendarData[m] / 0x10000) + 1) {
                cMonth = 1 - cMonth;
            }
            if (cMonth > Math.floor(CalendarData[m] / 0x10000) + 1) {
                cMonth--;
            }
        }
    }

    function GetcDateString() {

        //'<h3>农历腊月廿八</h3><h4>丙申年【猴年】庚寅月 戊午日</h4>'
        var tmp = "<h3>农历";

        if (cMonth < 1) {
            tmp += "(闰)";
            tmp += monString.charAt(-cMonth - 1);
        } else {
            tmp += monString.charAt(cMonth - 1);
        }
        tmp += "月";
        tmp += (cDay < 11) ? "初" : ((cDay < 20) ? "十" : ((cDay < 30) ? "廿" : "三十"));
        if (cDay % 10 != 0 || cDay == 10) {
            tmp += numString.charAt((cDay - 1) % 10);
        }
        tmp +="</h3><h4>";
        tmp += tgString.charAt((cYear - 4) % 10);
        tmp += dzString.charAt((cYear - 4) % 12);
        tmp += "年【";
        tmp += sx.charAt((cYear - 4) % 12);
        tmp += "年】 ";
        tmp+="</h4>";
        return tmp;
    }

    function GetLunarDay(solarYear, solarMonth, solarDay) {
        //solarYear = solarYear<1900?(1900+solarYear):solarYear;
        if (solarYear < 1921 || solarYear > 2020) {
            return "";
        } else {
            solarMonth = (parseInt(solarMonth) > 0) ? (solarMonth - 1) : 11;
            e2c(solarYear, solarMonth, solarDay);
            return GetcDateString();
        }
    }


    function showCal(AddDayCount){

        var D = new Date();
        D.setDate(D.getDate()+AddDayCount);
        var yy = D.getFullYear();
        var mm = D.getMonth() + 1;
        var dd = D.getDate();
        var ww = D.getDay();
        var ss = parseInt(D.getTime() / 1000);
        if (yy < 100) yy = "19" + yy;

        return GetLunarDay(yy,mm,dd);
    }
    /**
     * Created by Jerry on 17/10/31.
     */
    function GetCurrentDateTime(AddDayCount) {
        var d = new Date();
        d.setDate(d.getDate()+AddDayCount);
        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        var date = d.getDate();
        var week = d.getDay();

        var curDateTime = year;
        if (month > 9)
            curDateTime = curDateTime + "年" + month;
        else
            curDateTime = curDateTime + "年0" + month;
        if (date > 9)
            curDateTime = curDateTime + "月" + date + "日";
        else
            curDateTime = curDateTime + "月0" + date + "日";

        var weekday = "";
        if (week == 0)
            weekday = "日";
        else if (week == 1)
            weekday = "一";
        else if (week == 2)
            weekday = "二";
        else if (week == 3)
            weekday = "三";
        else if (week == 4)
            weekday = "四";
        else if (week == 5)
            weekday = "五";
        else if (week == 6)
            weekday = "六";
        curDateTime ='<h2>'+ curDateTime + " 周" + weekday +'</h2><h1>'+date+'</h1>';

        return curDateTime;
    }
    // JavaScript Document
    function slidedata(i){
        return '<div class="swiper-slide">' +
            '<div class="wannianli-card">' +
            '<div class="wannianli-info">'+GetCurrentDateTime(i)+showCal(i)+'</div>' +
            '<i class="wannianli-ico wannianli-lt"></i>' +
            '<i class="wannianli-ico wannianli-rt"></i>' +
            '<i class="wannianli-ico wannianli-rb"></i>' +
            '</div>' +
            '</div>';
    }
    function sliderMonth(i) {
        var _slide = document.createElement("div");  // 表格区 显示数据
        _slide.className = 'swiper-slide'
        var _card = document.createElement('div');
        _card.className = 'wannianli-card'
        _slide.appendChild(_card);
        var _info = document.createElement('div');
        _info.className = 'wannianli-info'
        _card.appendChild(_info);
        _info.appendChild(SliderrenderHtml(i))

        var _other = document.createElement('div');
        _other.className = 'wannianli-other'
        _card.appendChild(_other);

        var i1 = document.createElement('i');
        i1.className = 'wannianli-ico wannianli-lt'
        var i2 = document.createElement('i');
        i2.className = 'wannianli-ico wannianli-rt'
        var i3 = document.createElement('i');
        i3.className = 'wannianli-ico wannianli-rb'
        _card.appendChild(i1)
        _card.appendChild(i2)
        _card.appendChild(i3)
        // _slide.innerHTML =  SliderrenderHtml(i);
        // _slide.appendChild(SliderrenderHtml(i))
        // console.log('11111', _slide.innerHTML)

        return _slide
    }

    var mySwiper = new Swiper('.swiper-container', {
        roundLengths : true,
        initialSlide :2,
        speed:600,
        slidesPerView:"auto",
        centeredSlides : true,
        followFinger : false,
    })
    today=0;//默认显示今天
    pre=2;
    next=2;
    // mySwiper.appendSlide(slidedata(today));
    // mySwiper.appendSlide(slidedata(today+1));
    // mySwiper.appendSlide(slidedata(today+2));
    // mySwiper.prependSlide(slidedata(today-1));
    // mySwiper.prependSlide(slidedata(today-2));

    //月份
    var date = dateObj.getDate();
    mySwiper.appendSlide(sliderMonth(today))
    mySwiper.appendSlide(sliderMonth(today+1));
    mySwiper.appendSlide(sliderMonth(today+1));

// var date = dateObj.getDate();
    dateObj.setDate(new Date(date.getFullYear(), date.getMonth(), 1));
    mySwiper.prependSlide(sliderMonth(today-1));

    // var date = dateObj.getDate();
    // dateObj.setDate(new Date(date.getFullYear(), date.getMonth()+1, 1));

    // var date = dateObj.getDate();
    // dateObj.setDate(new Date(date.getFullYear(), date.getMonth()-1, 1));
    mySwiper.prependSlide(sliderMonth(today-1));

    mySwiper.on('slideChangeStart',function(swiper){
        //swiper.params.allowSwipeToPrev = false;
        swiper.lockSwipes();

    })

    mySwiper.on('slideChangeEnd',function(swiper){
        //alert(swiper.activeIndex);
        swiper.unlockSwipes();
        console.log('swiper.activeIndex', swiper.activeIndex,
            swiper.activeIndex,
            swiper.slides.length-2)
        if(swiper.activeIndex==1){
            pre++;
            dateObj.setDate(new Date(date.getFullYear(), date.getMonth(), 1));
            swiper.prependSlide(sliderMonth(today-pre));
        }
        // if(swiper.activeIndex==0){
        //     pre++;
        //     swiper.prependSlide(sliderMonth(today-pre));
        //     pre++;
        //     swiper.prependSlide(sliderMonth(today-pre));
        // }

        if(swiper.activeIndex==swiper.slides.length-2){
            next++;
            dateObj.setDate(new Date(date.getFullYear(), date.getMonth(), 1));
            swiper.appendSlide(sliderMonth(today+next));
        }
    //swiper.params.allowSwipeToPrev = true;
    })



    // 设置calendar div中的html部分
    // renderHtml();
    // 表格中显示日期
    // showCalendarData();
    // 绑定事件
    // bindEvent();


    function SliderrenderHtml(i) {
        var date = dateObj.getDate();
        dateObj.setDate(new Date(date.getFullYear(), date.getMonth()+i, 1));
        // console.log()
        var _headHtml = "<tr>" +
        "<th>日</th>" +
        "<th>一</th>" +
        "<th>二</th>" +
        "<th>三</th>" +
        "<th>四</th>" +
        "<th>五</th>" +
        "<th>六</th>" +
        "</tr>";
        var _bodyHtml = "";
        // 一个月最多31天，所以一个月最多占6行表格
        for(var i = 0; i < 6; i++) {
            _bodyHtml += "<tr>" +
                "<td></td>" +
                "<td></td>" +
                "<td></td>" +
                "<td></td>" +
                "<td></td>" +
                "<td></td>" +
                "<td></td>" +
                "</tr>";
         }
        var _div =document.createElement('div')
        var _table = document.createElement("table");  // 表格区 显示数据
        _table.className = 'calendar-table'
        _table.innerHTML =  '<table class="calendar-table">'+_headHtml + _bodyHtml+'</table>' ;
        // console.log(_table.getElementsByTagName("td"))
        var _tds = _table.getElementsByTagName("td")
        var _year = dateObj.getDate().getFullYear();
        var _month = dateObj.getDate().getMonth() + 1;
        var _dateStr = getDateStr(dateObj.getDate());

        // 设置顶部标题栏中的 年、月信息
        var _title = document.createElement('div');
        _title.className = "title";
        var titleStr = _dateStr.substr(0, 4) + "年" + _dateStr.substr(4,2) + "月";
        _title.innerHTML = titleStr;
        // console.log(_title.innerHTML)
        _div.appendChild(_title)
        _div.appendChild(_table)
        // 设置表格中的日期数据
        // var _table = document.getElementById("calendarTable");
        // var _tds = _table.getElementsByTagName("td");
        var _firstDay = new Date(_year, _month - 1, 1);  // 当前月第一天
        // console.log('当前月第一天', _firstDay, _firstDay.getDay())
        for(var i = 0; i < _tds.length; i++) {
            var _thisDay = new Date(_year, _month - 1, i + 1 - _firstDay.getDay());
            // console.log('_thisDay', _thisDay,_tds.length)
            var _thisDayStr = getDateStr(_thisDay);
            // console.log('_thisDayStr', _thisDayStr,getDateStr(new Date()))

            _tds[i].innerHTML = '<span>'+_thisDay.getDate()+'</span>';
            //_tds[i].data = _thisDayStr;
            _tds[i].setAttribute('data-index', _thisDayStr,);
            //<div class="table_time"><span>10am</span><span>--</span><span>12am</span></div>hasCourse
            if(_thisDayStr == getDateStr(new Date())) {    // 当前天
                _tds[i].className = 'currentDay';
                _tds[i].innerHTML = '<span class="mon_t">'+_thisDay.getDate()+'</span><div class="table_time">' +
                    '<div class="t"><span>10:00</span>' +
                    '</div>' +
                    '<div class="t"><span>--</span></div>' +
                    '<div class="t"><span>12:00</span>' +
                    '</div></div>';
            } else if (_thisDayStr == '20171101') {
                _tds[i].className = 'currentMonth ';  // 当前月
                _tds[i].innerHTML = '<span class="mon_t">'+_thisDay.getDate()+'</span><div class="table_time">' +
                    '<div class="t"><span>11:00</span>' +
                    '</div>' +
                    '<div class="t"><span>--</span></div>' +
                    '<div class="t"><span>13:00</span>' +
                    '</div></div>';
            } else if (_thisDayStr == '20171123') {
                _tds[i].className = 'currentMonth ';  // 当前月
                _tds[i].innerHTML = '<span class="mon_t">'+_thisDay.getDate()+'</span><div class="table_time">' +
                    '<div class="t"><span>14:00</span>' +
                    '</div>' +
                    '<div class="t"><span>--</span></div>' +
                    '<div class="t"><span>16:00</span>' +
                    '</div></div>';
            } else if(_thisDayStr.substr(0, 6) == getDateStr(_firstDay).substr(0, 6)) {
                _tds[i].className = 'currentMonth';  // 当前月
            }else {    // 其他月
                _tds[i].className = 'otherMonth';
            }
        }
        // console.log(_title.innerHTML + _table.innerHTML)
        return  _div;
    // bodyBox.innerHTML = "<table id='calendarTable' class='calendar-table'>" +
    //     _headHtml + _bodyHtml +
    //     "</table>";
    // 添加到calendar div中
    // calendar.appendChild(bodyBox);
    }
    /**
     * 渲染html结构
     */
    function renderHtml() {
        var calendar = document.getElementById("calendar");
        var titleBox = document.createElement("div");  // 标题盒子 设置上一月 下一月 标题
        var bodyBox = document.createElement("div");  // 表格区 显示数据

        // 设置标题盒子中的html
        titleBox.className = 'calendar-title-box';
        titleBox.innerHTML = "<span class='prev-month' id='prevMonth'></span>" +
            "<span class='calendar-title' id='calendarTitle'></span>" +
            "<span id='nextMonth' class='next-month'></span>";
        calendar.appendChild(titleBox);    // 添加到calendar div中

        // 设置表格区的html结构
        bodyBox.className = 'calendar-body-box';
        var _headHtml = "<tr>" +
            "<th>日</th>" +
            "<th>一</th>" +
            "<th>二</th>" +
            "<th>三</th>" +
            "<th>四</th>" +
            "<th>五</th>" +
            "<th>六</th>" +
            "</tr>";
        var _bodyHtml = "";

        // 一个月最多31天，所以一个月最多占6行表格
        for(var i = 0; i < 6; i++) {
            _bodyHtml += "<tr>" +
                "<td></td>" +
                "<td></td>" +
                "<td></td>" +
                "<td></td>" +
                "<td></td>" +
                "<td></td>" +
                "<td></td>" +
                "</tr>";
        }
        bodyBox.innerHTML = "<table id='calendarTable' class='calendar-table'>" +
            _headHtml + _bodyHtml +
            "</table>";
        // 添加到calendar div中
        calendar.appendChild(bodyBox);
    }

    /**
     * 表格中显示数据，并设置类名
     */
    function showCalendarData() {
        var _year = dateObj.getDate().getFullYear();
        var _month = dateObj.getDate().getMonth() + 1;
        var _dateStr = getDateStr(dateObj.getDate());

        // 设置顶部标题栏中的 年、月信息
        var calendarTitle = document.getElementById("calendarTitle");
        var titleStr = _dateStr.substr(0, 4) + "年" + _dateStr.substr(4,2) + "月";
        calendarTitle.innerText = titleStr;

        // 设置表格中的日期数据
        var _table = document.getElementById("calendarTable");
        var _tds = _table.getElementsByTagName("td");
        var _firstDay = new Date(_year, _month - 1, 1);  // 当前月第一天
        // console.log('当前月第一天', _firstDay, _firstDay.getDay())
        for(var i = 0; i < _tds.length; i++) {
            var _thisDay = new Date(_year, _month - 1, i + 1 - _firstDay.getDay());
            // console.log('_thisDay', _thisDay,_tds.length)
            var _thisDayStr = getDateStr(_thisDay);
            // console.log('_thisDayStr', _thisDayStr,getDateStr(new Date()))

            _tds[i].innerText = _thisDay.getDate();
            //_tds[i].data = _thisDayStr;
            _tds[i].setAttribute('data', _thisDayStr,);
            if(_thisDayStr == getDateStr(new Date())) {    // 当前天
                _tds[i].className = 'currentDay';
            }else if(_thisDayStr.substr(0, 6) == getDateStr(_firstDay).substr(0, 6)) {
                _tds[i].className = 'currentMonth';  // 当前月
            }else {    // 其他月
                _tds[i].className = 'otherMonth';
            }
        }
    }

    /**
     * 绑定上个月下个月事件
     */
    function bindEvent() {
        var prevMonth = document.getElementById("prevMonth");
        var nextMonth = document.getElementById("nextMonth");
        addEvent(prevMonth, 'click', toPrevMonth);
        addEvent(nextMonth, 'click', toNextMonth);
    }

    /**
     * 绑定事件
     */
    function addEvent(dom, eType, func) {
        if(dom.addEventListener) {  // DOM 2.0
            dom.addEventListener(eType, function(e){
                func(e);
            });
        } else if(dom.attachEvent){  // IE5+
            dom.attachEvent('on' + eType, function(e){
                func(e);
            });
        } else {  // DOM 0
            dom['on' + eType] = function(e) {
                func(e);
            }
        }
    }

    /**
     * 点击上个月图标触发
     */
    function toPrevMonth() {
        var date = dateObj.getDate();
        dateObj.setDate(new Date(date.getFullYear(), date.getMonth() - 1, 1));
        showCalendarData();
    }

    /**
     * 点击下个月图标触发
     */
    function toNextMonth() {
        var date = dateObj.getDate();
        dateObj.setDate(new Date(date.getFullYear(), date.getMonth() + 1, 1));
        showCalendarData();
    }

    /**
     * 日期转化为字符串， 4位年+2位月+2位日
     */
    function getDateStr(date) {
        var _year = date.getFullYear();
        var _month = date.getMonth() + 1;    // 月从0开始计数
        var _d = date.getDate();

        _month = (_month > 9) ? ("" + _month) : ("0" + _month);
        _d = (_d > 9) ? ("" + _d) : ("0" + _d);
        return _year + _month + _d;
    }
