/**
 * Created by Jerry on 17/11/16.
 */
window.urlPrefix = window.urlPrefix || '';

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
        return unescape(r[2]);
    return "";
}

function defaultValidationConfig() {
    return {
        promptPosition: 'centerRight', //设置提示位置为中右侧
        autoPositionUpdate: true,
        showOneMessage: false,
        binded: false, //取消即时验证
        autoHidePrompt: true, //自动隐藏提示信息
        autoHideDelay: 1500, //设置隐藏提示信息的延迟时间(ms)
        focusFirstField: false, //验证未通过时，是否给第一个不通过的控件获取焦点
        scroll: false // 屏幕自动滚动到第一个验证不通过的位置
    }
}

window.MOBILE_REG = /^((13[0-9])|(14[5,7])|(15[^4,\D])|(17[0-9])|(18[0-9]))\d{8}$/

function amazeValidator(config) {
    return $.extend({
        patterns: {
            mobileNo: MOBILE_REG
        }
    }, config);
}


if ($.validationEngineLanguage) {
    $.validationEngineLanguage.allRules['mobileNo'] = {
        // credit: jquery.h5validate.js / orefalo
        "regex": window.MOBILE_REG,
        "alertText": "* 无效的手机号码"
    };
}

function addLoading() {
    if ($('body').find('.uil-spin-css').length) {
        return;
    }
    var sHtml = "<div class='uil-spin-css'><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div></div><div class='uil-spin-css-backdrop modal-backdrop in'></div>";
    $('body').append(sHtml);
}

function removeLoading() {
    $('.uil-spin-css').remove();
    $('.uil-spin-css-backdrop').remove();
}

var Message = {
    success: function (msg, cb, time) {
        this.addWrap('success', msg, cb, time);
    },
    warn: function (msg, cb, time) {
        this.addWrap('warn', msg, cb, time);
    },
    error: function (msg, cb, time) {
        this.addWrap('danger', msg, cb, time);
    },
    tip: function (msg, cb, time) {
        this.addWrap(null, msg, cb, time);
    },
    init: function () {
        var self = this;
        if (!$('body').find('.m-messageWrap').length) {
            $('body').append('<div class="m-messageWrap"></div>');
        }
    },
    addWrap: function (type, text, cb, time) {
        var self = this;
        self.init();
        var alert = $('<div class="am-alert ' + (type ? ('am-alert-' + type ) : '') + '" data-am-alert>' +
            '<button type="button" class="am-close">&times;</button>' +
            '<p></p>' +
            '</div>');
        alert.find('p').append(text);

        $('.m-messageWrap').append(alert);

        alert.on('closed.alert.amui', function (e) {
            if ($.isFunction(cb)) {
                cb.call(this, e);
            }
        });
        alert.alert();
        setTimeout(function () {
            alert.alert('close');
        }, time || 2000)
    }
};

function handleResponse(rs, cb, ctx, showMsg) {
    if (rs.code == 0) {
        return true;
    }
    if (rs.code == 4) {
        //没有登录
        if (showMsg !== false) {
            (ctx || window).Message.error(rs.msg || "请先登录后再继续");
        }
        (ctx || window).showLogin();
        return false;
    }
    if (showMsg !== false) {
        (ctx || window).Message.error(rs.msg || "请求错误，请检查您的网络情况", cb);
    }
    return false;
}

/**
 * URL HASH PARAMETER
 * @constructor
 */
var HashParam = function () {
    this.parse();
    this.eventMap = {};
};
HashParam.prototype.parse = function () {
    this.params = {};
    if (!location.hash || location.hash == '#') {
        return;
    }
    var hash = location.hash;
    while (hash.indexOf('#') == 0) {
        hash = hash.substring(1);
    }
    if (!hash) {
        return;
    }
    var queryString = decodeURIComponent(hash);
    var pairs = queryString.split('&');
    if (pairs.length == 0) {
        return;
    }
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i], sp = pair.indexOf('='), key, val;
        if (sp == 0) {
            continue;
        } else if (sp < 0) {
            if (!this.params[pair]) {
                this.params[pair] = null;
            }
        } else if (sp == pair.length - 1) {
            if (!this.params[pair.substring(0, sp)]) {
                this.params[pair.substring(0, sp)] = null;
            }
        } else {
            key = pair.substring(0, sp);
            val = pair.substring(sp + 1);
            var origin = this.params[key];
            if ($.isArray(origin)) {
                origin.push(val);
            } else if (origin) {
                this.params[key] = [origin, val];
            } else {
                this.params[key] = val;
            }
        }
    }
};
HashParam.prototype.setInternal = function () {
    if (arguments.length >= 2) {
        var key = arguments[0], val = arguments[1];
        if (val == null) {
            return this.remove(key, null, true);
        } else {
            var origin = this.params[key];
            if (!origin) {
                this.params[key] = val;
                return true;
            } else if (origin == val) {
                return false;
            } else {
                var equal = true;
                if ($.isArray(origin) && $.isArray(val)) {
                    if (origin.length == val.length) {
                        for (var i = 0; i < origin.length; i++) {
                            var contain = false;
                            for (var j = 0; j < val.length; j++) {
                                if (origin[i] == val[j]) {
                                    contain = true;
                                    break;
                                }
                            }
                            if (!contain) {
                                equal = false;
                                break;
                            }
                        }
                    } else {
                        equal = false;
                    }
                } else {
                    equal = false;
                }
                if (equal) {
                    return false;
                }
            }
            this.params[key] = val;
            return true;
        }
    } else if (arguments.length == 1) {
        if ($.isPlainObject(arguments[0])) {
            var change = false;
            for (var key in arguments[0]) {
                change |= this.setInternal(key, arguments[0][key]);
            }
            return change;
        } else {
            return this.remove(arguments[0], null, true);
        }
    }
    return false;
};
HashParam.prototype.set = function () {
    if (this.setInternal.apply(this, arguments)) {
        this.update();
    }
};
HashParam.prototype.add = function (key, value) {
    var origin = this.params[key];
    if (origin) {
        this.params[key] = [origin, value];
    } else {
        this.params[key] = value;
    }
    this.update();
};
HashParam.prototype.remove = function (key, value, silence) {
    if (value == null || typeof value == 'undefined') {
        if (key in this.params) {
            delete this.params[key];
            this.update(silence);
            return true;
        }
    }
    var origin = this.params[key];
    if (origin) {
        if ($.isArray(origin)) {
            for (var i = 0; i < origin.length; i++) {
                var item = origin[i];
                if (item == value) {
                    origin.splice(i, 1);
                    if (origin.length == 0) {
                        delete this.params[key];
                    }
                    this.update(silence);
                    return true;
                }
            }
        } else if (value == origin) {
            delete this.params[key];
            this.update(silence);
            return true;
        }
    }
    return false;
};
HashParam.prototype.get = function (key) {
    return this.params[key];
};
HashParam.prototype.getJoin = function (key, join) {
    var result = this.params[key];
    if ($.isArray(result)) {
        return result.join(join);
    }
    return result;
};
HashParam.prototype.getFirst = function (key) {
    var val = this.params[key];
    if ($.isArray(val) && val.length > 0) {
        return val[0];
    } else {
        return val;
    }
};
HashParam.prototype.update = function (silence) {
    var hash = "", first = true, value;
    for (var key in this.params) {
        if (!first) {
            hash += '&';
        }
        first = false;
        value = this.params[key];
        if ($.isArray(value)) {
            if (value.length == 0) {
                continue;
            }
            for (var i = 0; i < value.length; i++) {
                var item = value[i];
                hash += key + '=' + item;
                if (i != value.length - 1) {
                    hash += '&';
                }
            }
        } else {
            hash += key + '=' + value;
        }
    }
    location.hash = hash;
    if (silence !== true) {
        this.fire('change');
    }
};
HashParam.prototype.on = function (event, callback) {
    var queue = this.eventMap[event];
    if (!queue) {
        queue = [];
        this.eventMap[event] = queue;
    }
    queue.push(callback);
};
HashParam.prototype.un = function (event, callback) {
    var queue = this.eventMap[event];
    if (!queue) {
        return;
    }
    for (var i = 0; i < queue.length; i++) {
        var cb = queue[i];
        if (cb == callback) {
            queue.splice(i, 1);
            i--;
        }
    }
};
HashParam.prototype.fire = function (event, args) {
    var queue = this.eventMap[event];
    if (!queue) {
        return;
    }
    for (var i = 0; i < queue.length; i++) {
        var cb = queue[i];
        if ($.isFunction(cb)) {
            cb.apply(this, args);
        }
    }
};

(function ($) {

    $.fn.loadJSON = function (url, param, success, loading) {
        var $this = this;
        if (loading !== false) {
            addLoading(this);
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: param,
            dataType: 'JSON',
            success: function (rs) {
                if (handleResponse(rs)) {
                    if ($.isFunction(success)) {
                        success.call($this, rs);
                    }
                }
            }, error: function (req) {
                Message.error(req.responseText || 'Server process request error');
            }, complete: function () {
                if (loading !== false) {
                    removeLoading($this);
                }
            }
        });
        return this;
    };

    $.fn.loadHtml = function (url, param, success, loading, silent) {
        var $this = this;
        if (loading !== false) {
            addLoading(this);
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: param,
            dataType: 'html',
            success: function (rs) {
                $this.each(function () {
                    $(this).html(rs);
                });
                if ($.isFunction(success)) {
                    success.call($this);
                }
            }, error: function (req) {
                if (silent === true) {
                    return;
                }
                if (req.status == 600) {
                    //service error
                    Message.error(req.responseText || 'Server process request error');
                } else if (req.status == 401) {
                    //login
                    showLogin()
                } else if (req.status == 404) {
                    //login
                    Message.error(req.responseText || 'The resource your are requesting was not existed');
                } else {
                    Message.error(req.responseText || 'Server process request error');
                }
            }, complete: function () {
                if (loading !== false) {
                    removeLoading($this);
                }
            }
        });
        return this;
    };


    $.isDomObject = function (obj) {
        var isDOM = ( typeof HTMLElement === 'object' ) ?
            function (obj) {
                return obj instanceof HTMLElement;
            } :
            function (obj) {
                return obj && typeof obj === 'object' && obj.nodeType === 1 && typeof obj.nodeName === 'string';
            };
        return isDOM(obj);
    }

    var defaultAjaxOpt = {
        dataType: 'JSON',
        type: 'POST',
        loading: true
    };
    /**
     * 对JQUERY的ajax请求包装一下，默认发送json post请求，并且主动添加loading
     * @param options
     * @returns {*}
     */
    $.ajaxExt = function (options) {
        var opt = $.extend({}, defaultAjaxOpt, options);
        if (opt.loading) {
            addLoading();
            var originComplete = opt.complete;
            opt.complete = function () {
                removeLoading();
                if ($.isFunction(originComplete)) {
                    originComplete.apply(this, arguments);
                }
            }
        }
        //json请求判断
        var dataType = opt.dataType ? opt.dataType.toString().toLowerCase() : '';
        if (dataType == 'json' || dataType == 'jsonp' || !opt.error) {
            var originError = opt.error;
            opt.error = function () {
                Message.error("请求服务器错误，请检查网络或稍后再试");
                if ($.isFunction(originError)) {
                    originError.apply(this, arguments);
                }
            };
        }
        if (opt.data) {
            var $obj = null;
            if (opt.data instanceof jQuery) {
                $obj = opt.data;
            } else if ($.isDomObject(opt.data)) {
                $obj = $(opt.data);
            }
            if ($obj && $obj.is('form')) {
                //表单直接序列化
                opt.data = $obj.serialize();
            }
        }
        return $.ajax(opt);
    }


    window.Modal = {
        modalId: 0,
        defModalOption: {
            closeViaDimmer: false
        },
        open: function (el, options) {
            var $el;
            if ($.isDomObject(el)) {
                $el = $(el).clone();
            } else if (el instanceof $) {
                $el = el.clone();
            } else {
                $el = $(el);
                if ($el.parents('body').length > 0) {
                    $el = $el.clone();
                }
            }
            if ($el.length == 0) {
                return $el;
            }
            if (!$el.is('.am-modal')) {
                var modal = $('<div class="am-modal am-modal-confirm" tabindex="-1"><div class="am-modal-dialog"></div></div>');
                modal.find('am-modal-dialog').append($el);
                $el = modal;
            }
            $el.attr('id', 'dynamic-modal-' + (++this.modalId));
            $('body').append($el);
            $el.modal($.extend(this.defModalOption, options));
            $el.on('closed.modal.amui', function () {
                $el.remove();
            });
            return $el;
        },
        show: function (msg, title) {
            var modal = $('<div class="am-modal am-modal-confirm" tabindex="-1">\n' +
                '    <div class="am-modal-dialog">\n' +
                '        <div class="am-modal-hd"></div>' +
                '        <div class="am-modal-bd"></div>\n' +
                '        <div class="am-modal-footer">\n' +
                '            <span class="am-modal-btn" data-am-modal-confirm>确定</span>\n' +
                '        </div>\n' +
                '    </div>\n' +
                '</div>');
            if (title) {
                modal.find('.am-modal-hd').append(title);
            } else {
                modal.find('.am-modal-hd').remove();
            }
            modal.find('.am-modal-bd').append(msg);
            return this.open(modal, {
                closeViaDimmer: true
            });
        },
        confirm: function (tip, onOk, onCancel) {
            var modal = $('<div class="am-modal am-modal-confirm" tabindex="-1">\n' +
                '    <div class="am-modal-dialog">\n' +
                '        <div class="am-modal-bd"></div>\n' +
                '        <div class="am-modal-footer">\n' +
                '            <span class="am-modal-btn" data-am-modal-cancel>取消</span>\n' +
                '            <span class="am-modal-btn" data-am-modal-confirm>确定</span>\n' +
                '        </div>\n' +
                '    </div>\n' +
                '</div>');
            modal.find('.am-modal-bd').append(tip);
            return this.open(modal, {
                onConfirm: function () {
                    if ($.isFunction(onOk)) {
                        onOk.call(this);
                    }
                },
                onCancel: function () {
                    if ($.isFunction(onCancel)) {
                        onCancel.call(this);
                    }
                }
            })
        }
    };

})(jQuery);

(function () {
    var loginModal;
    $(document).on('closed.modal.amui', function (e) {
        $(e.target).removeData('amui.modal');
    });
    window.showLogin = function (redirect) {
        if (loginModal) {
            loginModal.find('#login-box').trigger('click');
        } else {
            loginModal = $('#login-modal').on('closed.modal.amui', function () {
                loginModal = null;
            }).modal({
                closeViaDimmer: false,
                width: 430
            });
        }
        loginModal.data('redirect', redirect);
    }
    window.showRegister = function () {
        if (loginModal) {
            loginModal.find("a[href = '#register-box']").trigger('click');
            return;
        }
        loginModal = $('#login-modal').on('open.modal.amui', function () {
        }).on('closed.modal.amui', function () {
            loginModal = null;
        }).modal({
            closeViaDimmer: false,
            width: 430
        });
        loginModal.find("a[href = '#register-box']").trigger('click');
    }
    $('#register-btn').click(function (e) {
        e.preventDefault();
        showRegister();
    });
    $('#login-btn').click(function (e) {
        e.preventDefault();
        showLogin();
    });
    if ($('#login-frm').length) {
        $('#login-frm').validationEngine(defaultValidationConfig());
        $('#login-frm').submit(function () {
            var $form = $(this);
            if ($form.validationEngine('validate')) {
                $.ajaxExt({
                    url: '/login.do',
                    data: $form,
                    success: function (rs) {
                        if (handleResponse(rs)) {
                            var redirect = $form.parents('.am-modal').data('redirect');
                            if (redirect) {
                                location.href = redirect;
                            } else {
                                location.reload(true);
                            }
                        }
                    },
                    dataType: 'JSON',
                    type: 'POST'
                });
            }
            return false;
        });

        $('#login-frm .lg-reg-btn').click(function () {
            $('#login-frm').submit();
        });
    }
    if ($('#register-frm').length) {
        $('#register-frm').validationEngine(defaultValidationConfig());
        $('#register-frm').submit(function () {
            var $form = $(this);
            if ($form.validationEngine('validate')) {
                $.post('/user/register.do', $form.serializeArray(), function (rs) {
                    if (handleResponse(rs)) {
                        var redirect = $form.parents('.am-modal').data('redirect');
                        if (redirect) {
                            location.href = redirect;
                        } else {
                            location.reload(true);
                        }
                    }
                }, 'JSON');
            }
            return false;
        });

        $('#register-frm .lg-reg-btn').click(function () {
            $('#register-frm').submit();
        });
    }
    //点击忘记密码关闭之前的登录注册模态框
    $('.forget-password-btn').click(function () {
        if (loginModal) {
            loginModal.modal('close');
        }
        $('#forgot-password-modal form').each(function () {
            this.reset();
        })
        $('#forgot-password-modal').modal({closeViaDimmer: 0, width: 430});
    });
    if ($('#reset-pwd-first').length && $('#reset-pwd-second').length) {
        $('#reset-pwd-first')
            .submit(function () {
                var $form = $(this);
                if ($form.validationEngine('validate')) {
                    $.ajaxExt({
                        url: '/user/start_reset.do',
                        data: $form,
                        success: function (rs) {
                            if (handleResponse(rs)) {
                                $(".forgot-password-modal").hide();
                                $(".find-password-modal").show();
                            }
                        }
                    });
                }
                return false;
            })
            .validationEngine(defaultValidationConfig())
            .find('.lg-reg-btn').click(function () {
            $('#reset-pwd-first').submit();
        });

        $('#reset-pwd-second')
            .submit(function () {
                var $form = $(this);
                if ($form.validationEngine('validate')) {
                    $.ajaxExt({
                        url: '/user/reset.do',
                        data: $form,
                        success: function (rs) {
                            if (handleResponse(rs)) {
                                Message.success('密码修改成功', function () {
                                    location.reload(true);
                                });
                            }
                        }
                    });
                }
                return false;
            })
            .validationEngine(defaultValidationConfig())
            .find('.lg-reg-btn').click(function () {
            $('#reset-pwd-second').submit();
        });
    }

    $('.sms-btn').click(function () {
        var $this = $(this), text = $this.val(), $number = $this.parents('form').find('input[name=number]'),
            number = $.trim($number.val());
        if ($this.prop('disabled')) {
            return;
        }
        if (!number || !window.MOBILE_REG.test(number)) {
            Message.error('请输入有效的手机号码', function () {
                $number.focus();
            }, 1000);
            return;
        }
        var texter = $this.is('input') ? function (text) {
            $this.val(text)
        } : function (text) {
            $this.html(text)
        };
        $.ajaxExt({
            url: '/verify/sms.do',
            data: {number: number},
            type: 'POST',
            dataType: 'json',
            success: function (rs) {
                if (handleResponse(rs)) {
                    Message.success("短信已发送，请查收");
                    $this.prop('disabled', true);
                    var timeLeft = 60;
                    var timer = setInterval(function () {
                        if (timeLeft === 0) {
                            clearInterval(timer);
                            $this.prop('disabled', false);
                            texter(text);
                        } else {
                            texter(timeLeft + '秒后重新发送');
                            timeLeft--;
                        }
                    }, 1000);
                }
            }
        })
    });
    $('.drop-icon-button,.drop-menu').click(function () {
        var _this = $(this), menu;
        if (_this.is('.drop-menu')) {
            menu = _this;
        } else {
            menu = _this.parent().find('.drop-menu');
        }
        menu.toggle();
    });
    $('body').click(function (e) {
        var me = $(e.target);
        if (me.is('.drop-icon-button') || me.parents('.drop-icon-button').length > 0) {
            return;
        }
        $('.drop-menu').hide();
    })
    $('#custom-info').click(function (e) {
        e.stopPropagation();
        showLogin();
    });
    $('#qqkf').click(function () {
        window.open('http://wpa.qq.com/msgrd?v=3&uin=941552016&site=qq&menu=yes');
    })
    $('.am-tabs li:not(.active) a.tab-interactive-less').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        location.href = $(this).attr('href');
    })

})();
