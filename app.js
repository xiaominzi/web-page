var express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),

    middlewares = require('./middlewares'),
    config = require('./config'),
    app = express(),
    routes;


routes = require('./routes');

app.disable('x-powered-by');
app.set('env', process.env.NODE_ENV);
app.set('port', config.server.port);
app.set('views', __dirname + '/views');
app.engine('.html', require('ejs').__express);
app.set('view engine', 'html');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false, limit: '1000kb'}));
app.use(express.static(path.join(__dirname, config.server.public)));

// custom middlewares
app.use(middlewares.before);
// routes
app.use(routes);
// error handler
app.use(middlewares.after);

module.exports = app;
