module.exports = {
  before: [
      require('./httpLogger')
  ],
  after: [
    require('./notFoundHandler'),
    require('./errorHandler')
  ]
};
