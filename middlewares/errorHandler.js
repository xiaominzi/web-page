
module.exports = function (err, req, res, next) {

  if (err.status === 404) {
    res.status(404);
    res.render('404');
  } else {
    res.status(500);
    res.render('500');
  }


  next();
};
