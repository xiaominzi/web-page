var log4js = require('log4js'),
    config = require('../config');

module.exports = log4js.connectLogger(
    log4js.getLogger(config.logger.name),
    {
        level: 'INFO',
        format: ':method ":url" :status :res[Content-Length] :response-time'
    }
);
