#!/usr/bin/env node
var http = require('http'),

    config = require('../config'),
    app = require('../app'),
    port = app.get('port'),
    //sequelize = require('../models'),
    logger = require('../services/logger'),
    server;

server = http.createServer(app);

//sequelize
//    .sync()
//    .then(function () {
//
//        logger.debug('Sequelize sync success.');
//        server.listen(port);
//    })
//    .catch(function (err) {
//        logger.error('Sequelize sync error.');
//        logger.error(err);
//    });

server.listen(port);

server.on('error', onError);
server.on('listening', onListening);

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            logger.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            logger.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

function onListening() {
    var addr = server.address(),
        env = app.get('env'),
        bind = typeof addr === 'string'
            ? 'pipe ' + addr
            : 'port ' + addr.port;

    console.log('+++++++',env);

    logger.info('ENV is ' + (!env ? 'local' : env));
    logger.info('Listening on ' + bind);
}
